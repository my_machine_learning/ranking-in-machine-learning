#!/usr/bin/env python

import csv


def build():
    X = []
    Y = []

    arquivo = open('acesso.csv', 'rb')
    leitor = csv.reader(arquivo)
    
    # pula primeira linha
    leitor.next()

    for linha in leitor:
        # linha[0] - coluna 1
        # linha[1] - coluna 2
        # linha[2] - coluna 3
        # linha[3] - coluna 4 classe
        X.append([ int(linha[0]) , int(linha[1]) , int(linha[2]) ])
        Y.append(int(linha[3]))

    return X,Y
