import numpy as np

def real(modelo, validacao_dados, validacao_marcacoes):
    resultado = modelo.predict(validacao_dados)

    # torna uma lista de array em um array
    validacao_marcacoes = np.concatenate(validacao_marcacoes)
    acertos = resultado == validacao_marcacoes

    total_de_acertos = sum(acertos)
    total_de_elementos = len(validacao_marcacoes)

    taxa_de_acerto = 100.0 * total_de_acertos / total_de_elementos

    msg = "Taxa de acerto no mundo real: {0}".format(taxa_de_acerto)
    print(msg)