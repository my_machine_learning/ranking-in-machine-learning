import pandas as pd
import math
import numpy as np
from fit_and_predict import fit_and_predict
from real import real

 # x% dos dados
PORCENTAGEM_TREINO = 0.8
PORCENTAGEM_TESTE = 0.1

data_frame = pd.read_csv('buscas2.csv')

# RESULTADO - COLUNAS ESCOLHIDAS
# logado = 62.5
# logado e busca = 75
# home busca logado = 75

X_data_frame = data_frame[['home','busca','logado']]
Y_data_frame = data_frame[['comprou']]

# cria as categorias(dummies) para coluna busca 
# & converte os valores do data frame para inteiro
x_data_frame_dummies = pd.get_dummies(X_data_frame).astype(int)

# a coluna comprou nao precisa de passar por categorizacao
# convertemos de data frame para array
X = x_data_frame_dummies.values
Y = Y_data_frame .values

# tamanho das partes - treino teste local & avaliacao
tamanho_treino = int(PORCENTAGEM_TREINO * len(X))
# arredonda pra cima 
tamanho_teste = int(math.ceil(PORCENTAGEM_TESTE * len(X)))
tamanho_da_avaliacao = len(X) - tamanho_teste - tamanho_treino

# separa os dados para treino 
treino_dados = X[:tamanho_treino]
treino_marcacoes = Y[:tamanho_treino]
# separa os dados para teste
teste_dados = X[tamanho_treino : tamanho_treino+tamanho_teste]
teste_marcacoes = Y[tamanho_treino : tamanho_treino+tamanho_teste]

# separa os dados para avaliacao
avaliacao_dados = X[-tamanho_da_avaliacao:]
avaliacao_marcacoes = Y[-tamanho_da_avaliacao:] 

from sklearn.naive_bayes import MultinomialNB
modelo_bayes = MultinomialNB()
taxa_bayes = fit_and_predict(modelo_bayes, treino_dados, treino_marcacoes, teste_dados, teste_marcacoes)
print("Taxa do Modelo Naive Bayes : {0}".format(taxa_bayes))

from sklearn.ensemble import AdaBoostClassifier
modelo_ada = AdaBoostClassifier()
taxa_ada = fit_and_predict(modelo_ada, treino_dados, treino_marcacoes, teste_dados, teste_marcacoes)
print("Taxa do Modelo Ada Boost : {0}".format(taxa_ada))

if taxa_bayes >= taxa_ada:
    modelo_selecionado = modelo_bayes
else:
    modelo_selecionado = modelo_ada

real(modelo_selecionado,avaliacao_dados,avaliacao_marcacoes)